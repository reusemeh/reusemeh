from django.shortcuts import render
from listbarang.models import Barang
from accounts.models import Member
# Create your views here.


def tracker(request):
    user = request.user
    response = {}
    if(user.is_authenticated):
        barang_dijual = Barang.objects.filter(penjual=user)
        barang_dibeli = Barang.objects.filter(pembeli=user)
        response['barang_dijual'] = barang_dijual
        response['barang_dibeli'] = barang_dibeli
    return render(request, 'tracker.html', response)
