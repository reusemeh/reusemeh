from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class MyUserCreationForms(UserCreationForm):
    
    npm = forms.CharField(max_length=30, label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "NPM",
        }
    ))
    
    fullname = forms.CharField(max_length=30, label='',widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama lengkap",
        }
    ))

    username = forms.CharField(min_length=8, max_length=150, label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama pengguna",
        }
    ))

    email = forms.EmailField(max_length=30, label='', widget=forms.EmailInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Email",
        }
    ))

    address = forms.CharField(label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Alamat",
        }
    ))

    namaBank = forms.CharField(label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama Bank",
        }
    ))

    noRekening = forms.CharField(label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "No Rekening",
        }
    ))
    
    password1 = forms.CharField(min_length=8, max_length=150, label='', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Kata sandi",
        }
    ))

    password2 = forms.CharField(min_length=8, max_length=150, label='', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Konfirmasi kata sandi",
        }
    ))


    class Meta:
        model = User
        fields = ('npm','fullname', 'username', 'email', 'address', 'namaBank', 'noRekening', 'password1', 'password2')
    
    def save(self, commit=True):
        user = super(MyUserCreationForms,self).save(commit=False)
        user.npm = self.clean_data['npm']
        user.fullname = self.clean_data['fullname']
        user.email = self.clean_data['email']
        user.address = self.clean_data['address']
        user.namaBank = self.clean_data['namaBank']
        user.noRekening = self.clean_data['noRekening']

        if commit:
            user.save()

        return user

class MyAuthenticationForm(AuthenticationForm):
    
    username = forms.CharField(min_length=8, max_length=150, label='', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama pengguna",
        }
    ))

    password = forms.CharField(min_length=8, max_length=150, label='', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Kata sandi",
        }
    ))
