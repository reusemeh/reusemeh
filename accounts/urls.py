from django.urls import path
from .views import signupMember, loginMember, logoutMember

urlpatterns = [
    path('signup/', signupMember),
    path('login/', loginMember, name='login'),
    path('logout/', logoutMember),
]
