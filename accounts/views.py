from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MyUserCreationForms, MyAuthenticationForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.

def signupMember(request):
    if request.method == 'POST':
        form = MyUserCreationForms(request.POST)
        if form.is_valid():
            user = form.save()
            #User Logged On
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')

    else:
        form = MyUserCreationForms()
    return render(request, 'signupaccount.html', {'forms':form})

def loginMember(request):
    if request.method == "POST":
        form = MyAuthenticationForm(data=request.POST)
        if form.is_valid():
            #User Logged in
            user = form.get_user()
            login(request, user)
            request.session['username'] = user
            return HttpResponseRedirect('/')
    else:
        form = MyAuthenticationForm()
    return render(request, 'login.html', {'forms':form})

def logoutMember(request):
    logout(request)
    request.session.flush()
    return HttpResponseRedirect('/')