from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime
# Create your models here.

class Member(AbstractUser):
    npm = models.CharField(default='null', max_length=10)
    fullname = models.CharField(default='null', max_length=40)
    email = models.EmailField(default="null@gmail.com", unique=True)
    address = models.CharField(default='null', max_length=300)
    namaBank = models.CharField(default='null', max_length=300)
    noRekening = models.CharField(default='null', max_length=300)

    def __str__(self):
        return self.email
