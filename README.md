# Kelompok 8 PPW-J

## Anggota Kelompok

Berikut anggota kelompok 8.

1.  Saul Andre Lumban Gaol(1706023555)
    Fitur   :
    AppName :

2.  Monalisa Valencia Merauje (1706103543)
    Fitur   :
    AppName :

3.  Muzakki Hassan Azmi (1606917632)
    Fitur   :
    AppName :

4. Gusti Fahmi Fadhila (1706044036)
    Fitur   :
    AppName :

## Pipeline Status

[![pipeline status](https://gitlab.com/reusemeh/reusemeh/badges/master/pipeline.svg)](https://gitlab.com/reusemeh/reusemeh/commits/master)

## Code Coverage Status

[![coverage report](https://gitlab.com/reusemeh/reusemeh/badges/master/coverage.svg)](https://gitlab.com/reusemeh/reusemeh/commits/master)

## Link Herokuapp
https://reusemepls.herokuapp.com/