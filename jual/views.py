from django.shortcuts import render
from .forms import FormPenjualan
from listbarang.models import Barang
from accounts.models import Member

# Create your views here.

def jual(request):
    title = 'Jual Barang'
    form = FormPenjualan
    response = {'title':title}
    user = request.user
    if request.method == 'POST':
        form = FormPenjualan(request.POST)
        response['form'] = form
        if form.is_valid:
            barang = Barang(username = user.username,
            kategori = request.POST['kategori'],
            nama_barang = request.POST['nama_barang'],
            foto_barang = request.POST['foto_barang'],
            deskripsi_barang = request.POST['deskripsi_barang'],
            harga_barang = request.POST['harga_barang'],
            penjual=user,)
            barang.save()
            form = FormPenjualan
            response['form'] = form
    else:
        form = FormPenjualan()
        response['form'] = form
    return render(request, 'jual.html', response)