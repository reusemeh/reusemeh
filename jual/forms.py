from django import forms
from listbarang.models import Barang
from .choices import *

class FormPenjualan(forms.Form):
    kategori = forms.CharField(label='Kategori', widget=forms.Select(choices=CATEGORY))
    nama_barang = forms.CharField(label='Nama Barang', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    foto_barang = forms.Field(label='Link Foto Barang', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    deskripsi_barang = forms.Field(label='Deskripsi Barang', required=True, widget=forms.Textarea(attrs={'class': 'form-control'}))
    harga_barang = forms.IntegerField(label='Harga Barang (IDR)', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Barang