from django.test import TestCase, Client
from django.urls import resolve

from .views import listbarang
from .models import Barang

class ListbarangUnitTest(TestCase):

    def test_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_listbarang_function(self):
        found = resolve('/')
        self.assertEqual(found.func, listbarang)

    def test_template_is_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'listbarang.html')

    def test_model_can_create_new_barang(self):
        new_barang = Barang.objects.create(username='aku', email='kamu@yahoo.com', kategori='Home', nama_barang='saya', deskripsi_barang='kamunya kamu', harga_barang=100000)
        counting_all_barang = Barang.objects.all().count()
        self.assertEqual(counting_all_barang, 1)
    
    def test_tostring_return_nama_barang(self):
        new_barang = Barang.objects.create(username='aku', email='kamu@yahoo.com', kategori='Home', nama_barang='saya', deskripsi_barang='kamunya kamu', harga_barang=100000)
        self.assertEqual(str(new_barang), new_barang.nama_barang)