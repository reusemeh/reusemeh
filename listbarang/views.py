from django.shortcuts import render
from .models import Barang

response = {}


def listbarang(request):
    title = 'Reuse me, please'
    response = {'title': title}

    response['barang_list'] = Barang.objects.all()
    return render(request, 'listbarang.html', response)
