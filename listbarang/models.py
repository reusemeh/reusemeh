from django.db import models
from jual.choices import CATEGORY
from accounts.models import Member

class Barang(models.Model):
    username = models.CharField(max_length=30)
    email = models.CharField(max_length=100)
    kategori = models.IntegerField(choices=CATEGORY)
    nama_barang = models.CharField(max_length=100)
    foto_barang = models.TextField()
    deskripsi_barang = models.TextField()
    harga_barang = models.PositiveIntegerField()
    penjual = models.ForeignKey(Member, null=True, on_delete=models.CASCADE, related_name='penjual')
    pembeli = models.ForeignKey(Member, null=True, on_delete=models.CASCADE, related_name='pembeli')

    def __str__(self):
        return "%s %s" % (self.username, self.nama_barang)